//
//  FirstScreen.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 5/21/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import UIKit
import CoreData

var countriesSelected: [Country] = []

class FirstScreen: UIViewController {
    
    @IBOutlet weak var tableViewFirstSc: UITableView!
        
    @IBOutlet weak var noCountrySelected: UIView!
    
    @IBAction func AddCountryButtonPresed(_ sender: Any) {
       
       self.performSegue(withIdentifier: "SecondScreenSeque", sender: self)
            
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewFirstSc.delegate = self
        tableViewFirstSc.dataSource = self
        
        if(countriesSelected.count == 0)
        {
            tableViewFirstSc.isHidden = true
            noCountrySelected.isHidden = false
        }
        else
        {
            tableViewFirstSc.isHidden = false
            noCountrySelected.isHidden = true
        }
    
    }
    

}

    extension FirstScreen: UITableViewDelegate, UITableViewDataSource
    {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return countriesSelected.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let country = countriesSelected[indexPath.row]

            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell") as! FirstScreenCell

           cell.setInformation(country: country) 
            
            return cell

        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let myIndex = indexPath.row
            countrName = countriesSelected[myIndex].name
            
            var recovered, confirmed, deaths: Int
            var date1: Date

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Information")
                   
            request.returnsObjectsAsFaults = false
            
            do
            {
                let results = try context.fetch(request)
                                  
                if results.count > 0
                {
                        for result in results as! [NSManagedObject]
                            {
                                let cName  = result.value(forKey: "name") as! String
                                
                                if(cName == countrName)
                                {
                                    
                                    date1 = result.value(forKey: "date") as! Date
                                    
                                    confirmed = result.value(forKey: "confirmed") as! Int
                                    
                                    recovered = result.value(forKey: "recovered") as! Int
                                    
                                    deaths = result.value(forKey: "deaths") as! Int
                                    
                                    
                                    information.append(Information(name: cName, date: date1, confirmed: confirmed, recovered: recovered, deaths: deaths))
                                }
                    }
                }
            }
            catch{}
            
            
            
        self.performSegue(withIdentifier: "TimelineSeque", sender: self)
        }
        
        
        func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            
            if editingStyle == .delete
            {
                countriesSelected.remove(at: indexPath.row)
                
                tableView.deleteRows(at: [indexPath], with: .bottom)
            }
          
                if(countriesSelected.count == 0)
                {
                    tableViewFirstSc.isHidden = true
                    noCountrySelected.isHidden = false
                   
                }
                else
                {
                    tableViewFirstSc.isHidden = false
                    noCountrySelected.isHidden = true
                }
        }
}
