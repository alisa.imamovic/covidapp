//
//  CountryCellTableViewCell.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 5/20/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import UIKit

class CountryCellTableViewCell: UITableViewCell {

    
    @IBOutlet weak var CountryTitleLabel: UILabel!
    
    
    func setCountry(country: String)
    {
        CountryTitleLabel.text = country
    }
    
}
