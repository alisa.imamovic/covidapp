//
//  CountriesList.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 5/20/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import UIKit
import CoreData

var myIndex = 0

class CountriesList: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func XButton(_ sender: Any) {
        self.performSegue(withIdentifier: "BackToFirstScreenSeg", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tableView.delegate = self
        tableView.dataSource = self
        //deleteAll()
        //createCountries()
    }

    func deleteAll()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
                              
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Countries")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try context.execute(deleteRequest)
        }
        catch _ as NSError {}
    }
    
   func createCountries()
   {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
         
    let today = Date(timeIntervalSinceNow: TimeInterval(bitPattern: 0))
        let formatter = DateFormatter()
        let day = -3600*24
        formatter.dateFormat = "YYYY-MM-dd"
          
        let newCountry = NSEntityDescription.insertNewObject(forEntityName: "Countries", into: context)
        newCountry.setValue("Macedonia", forKey: "name")
    
    var countryInfo = NSEntityDescription.insertNewObject(forEntityName: "Information", into: context)
        countryInfo.setValue("Macedonia", forKey: "name")
        countryInfo.setValue(today, forKey: "date")
        countryInfo.setValue(268, forKey: "confirmed")
        countryInfo.setValue(112, forKey: "recovered")
        countryInfo.setValue(41, forKey: "deaths")
    
        do
        {
            try context.save()
        }
        catch{}
               
         countryInfo = NSEntityDescription.insertNewObject(forEntityName: "Information", into: context)
        countryInfo.setValue("Macedonia", forKey: "name")
        countryInfo.setValue(today.addingTimeInterval(TimeInterval(day)), forKey: "date")
           countryInfo.setValue(260, forKey: "confirmed")
           countryInfo.setValue(101, forKey: "recovered")
           countryInfo.setValue(39, forKey: "deaths")
    
        do
        {
            try context.save()
        }
        catch{}
    
        countryInfo = NSEntityDescription.insertNewObject(forEntityName: "Information", into: context)
        countryInfo.setValue("Macedonia", forKey: "name")
        countryInfo.setValue(today.addingTimeInterval(TimeInterval(day*2)), forKey: "date")
        countryInfo.setValue(249, forKey: "confirmed")
        countryInfo.setValue(97, forKey: "recovered")
        countryInfo.setValue(36, forKey: "deaths")
        do
        {
            try context.save()
        }
        catch{}
    
    
    
    countryInfo = NSEntityDescription.insertNewObject(forEntityName: "Information", into: context)
        countryInfo.setValue("Macedonia", forKey: "name")
        countryInfo.setValue(today.addingTimeInterval(TimeInterval(day*3)), forKey: "date")
        countryInfo.setValue(236, forKey: "confirmed")
        countryInfo.setValue(86, forKey: "recovered")
        countryInfo.setValue(33, forKey: "deaths")
        do
        {
            try context.save()
        }
        catch{}
    
    
        countryInfo = NSEntityDescription.insertNewObject(forEntityName: "Information", into: context)
        countryInfo.setValue("Macedonia", forKey: "name")
        countryInfo.setValue(today.addingTimeInterval(TimeInterval(day*4)), forKey: "date")
        countryInfo.setValue(221, forKey: "confirmed")
        countryInfo.setValue(80, forKey: "recovered")
        countryInfo.setValue(28, forKey: "deaths")
    
        do
        {
            try context.save()
        }
        catch{}
    
    }
}
    extension CountriesList: UITableViewDataSource, UITableViewDelegate
    {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
             let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Countries")
                   
            request.returnsObjectsAsFaults = false
            
            do
            {
                let results = try context.fetch(request)
                return results.count
            }
            catch{}
            
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let index = indexPath.row
            var countryName:String
            
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Countries")
                   
            request.returnsObjectsAsFaults = false
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell") as! CountryCellTableViewCell
                   
            do
            {
                let results = try context.fetch(request)
                       
                       if results.count > 0
                       {
                        var i = 0
                           for result in results as! [NSManagedObject]
                            {
                                if(i == index)
                                {
                                    countryName  = result.value(forKey: "name") as! String
                                    
                                        cell.setCountry(country: countryName)
                                        break
                                }
                                else
                                {
                                   i += 1
                                }
                            }
                        }
            }
            catch{}
            
          return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
            myIndex = indexPath.row
                               
            var countryName, name: String
            var confirmed, recovered, deaths: Int
            var date: Date
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request1 = NSFetchRequest<NSFetchRequestResult>(entityName: "Countries")
                   
            request1.returnsObjectsAsFaults = false
           
            let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Information")
                              
            request2.returnsObjectsAsFaults = false
                   
            do
            {
                let results = try context.fetch(request1)
                       
                       if results.count > 0
                       {
                        var i = 0
                           for result in results as! [NSManagedObject]
                            {
                                if(i == myIndex)
                                {
                                    countryName  = result.value(forKey: "name") as! String
                                    
                                    do
                                    {
                                        let infos = try context.fetch(request2)
                                    
                                    if infos.count > 0
                                    {
                                        for info in infos as! [NSManagedObject]
                                        {

                                        name = info.value(forKey: "name") as! String
                                            
                                        if(name == countryName)
                                        {
                                            date = info.value(forKey: "date") as! Date
                                            confirmed = info.value(forKey: "confirmed") as! Int
                                            recovered = info.value(forKey: "recovered") as! Int
                                            deaths = info.value(forKey: "deaths") as! Int
                                    
                                        countriesSelected.append(Country(name: countryName, date: date, confirmed: confirmed, recovered: recovered, deaths: deaths))
                                        
                                            break
                                        }
                                    }
                                 break
                                }
                                        
                                }
                                catch{ }
                                }
                                else
                                {
                                   i += 1
                                }
                            }
                        }
            }
            catch{}
    
            performSegue(withIdentifier: "BackToFirstScreenSeg", sender: self)
        }
        
        
        
        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return true
        }
        
    }
    
    


