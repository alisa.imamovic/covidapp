//
//  Information.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 6/15/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import Foundation


class Information
{
    var name: String
    var date: Date
    var confirmed: Int
    var recovered: Int
    var deaths: Int
    
    
    init(name: String, date: Date, confirmed: Int, recovered: Int, deaths: Int)
    {
        self.name = name
        self.date = date
        self.confirmed = confirmed
        self.recovered = recovered
        self.deaths = deaths
    }
}
