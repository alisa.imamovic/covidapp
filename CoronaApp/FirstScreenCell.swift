//
//  FirstScreenCell.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 5/21/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class FirstScreenCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var Confirmed: UILabel!
    
    @IBOutlet weak var Recovered: UILabel!
    
    @IBOutlet weak var Deaths: UILabel!
    
    
    @IBOutlet weak var date: UILabel!
    
    func setInformation(country: Country)
    {
        nameLabel.text = country.name
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        date.text = formatter.string(from: country.date)
                            
        Confirmed.text = "Confirmed: " + String(country.confirmed)
        Recovered.text = "Recovered: " + String(country.recovered)
        Deaths.text = "Deaths: " + String(country.deaths)
    }
    
    
    
}
