//
//  Country.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 5/20/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import Foundation
import UIKit 
import CoreData

class Country// NSManagedObject
{
    var name:  String
    var date: Date
    var confirmed: Int
    var recovered: Int
    var deaths: Int
    
    
    init(name: String, date: Date, confirmed: Int, recovered: Int, deaths: Int)
    {
       // super.init()
        //super.init(entity entity: NSEntityDescription, insertIntoManagedObjectContext, context: NSManagedObjectContext?)
      
       // let moc = NSManagedObjectContext()
       // let entity = NSEntityDescription.entity(forEntityName: "Country", in: moc)!

      //  super.init(entity: entity, insertInto: moc)

        self.name = name
        self.date = date
        self.confirmed = confirmed
        self.recovered = recovered
        self.deaths = deaths
    }
  

}
