//
//  TimelineCell.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 6/14/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import UIKit
import CoreData

class TimelineCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var confirmedLabel: UILabel!
    
    @IBOutlet weak var recoveredLabel: UILabel!
    
    @IBOutlet weak var deathsLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    func setCell(name: String, date: Date, confirmed: Int, recovered: Int, deaths: Int)
    {
        nameLabel.text = name
        
       let formatter = DateFormatter()
        formatter.dateStyle = .long
        dateLabel.text = formatter.string(from: date)
        
        confirmedLabel.text = "Confirmed: " + String(confirmed)
        recoveredLabel.text = "Recovered: " + String(recovered)
        deathsLabel.text = "Deaths: " + String(deaths)
        
    }
    
    
}
