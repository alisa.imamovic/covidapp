//
//  CountryTimeline.swift
//  CoronaApp
//
//  Created by Alisa Imamovic on 6/14/20.
//  Copyright © 2020 Alisa Imamovic. All rights reserved.
//

import Foundation
import UIKit
import CoreData

var countrName: String = " "
var information: [Information] = []

class CountryTimeline: UIViewController
{
    @IBOutlet weak var countryName: UILabel!
    
    @IBAction func goBack(_ sender: Any) {
        information.removeAll()
        self.performSegue(withIdentifier: "BackToFirst", sender: self)
    }
    
    @IBOutlet weak var TimelineTableView: UITableView!
    
    
    override func viewDidLoad() {
           super.viewDidLoad()
        
        countryName.text = countrName
        
        TimelineTableView.delegate = self
        TimelineTableView.dataSource = self
    }
}

extension CountryTimeline: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return information.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let myIndex = indexPath.row
            
         let cell = TimelineTableView.dequeueReusableCell(withIdentifier: "TimelineCell") as! TimelineCell
        
        cell.setCell(name: information[myIndex].name
            , date: information[myIndex].date,  confirmed: information[myIndex].confirmed, recovered: information[myIndex].recovered, deaths: information[myIndex].deaths)
                           
        return cell
    }
}
